import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css'],
})
export class ContadorComponent implements OnInit {
  public titulo: string = 'Contador App';
  public numero: number = 10;
  public base: number = 5;

  constructor() {}

  ngOnInit(): void {}

  acumular(valor: number) {
    this.numero += valor;
  }
}
