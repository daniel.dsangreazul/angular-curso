import { Component } from '@angular/core';
import { Personaje } from '../interfaces/dbz.interface';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css'],
})
export class MainPageComponent {
  nuevo: Personaje = {
    nombre: 'Daniel',
    poder: 20000,
  };

  // get personajes(): Personaje[] {
  //   return this.dbzService.personajes;
  // }

  constructor() {}
}
